import React from 'react';
import { DatePicker } from 'antd';
import './App.less';

const App = () => <DatePicker style={{ margin: 20 }} />;

export default App;
